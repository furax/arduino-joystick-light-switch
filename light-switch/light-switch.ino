#include "GyverTimer.h"
#include "microDS18B20.h"
#include "ModbusRtu.h"

GTimer blinker(MS);
MicroDS18B20<3> thermometer;
Modbus slave(21, 0, 2);

const int orientation = 0;
const int minMovement = 51;
const int maxMovement = 410;
const int movementCenter = 512;

const size_t modbusRegisterCount = 4;
uint16_t modbusRegisters[modbusRegisterCount];

const int16_t lightBlue = 1 << 8;
const int16_t lightGreen = 1 << 9;
const int16_t lightRed = 1 << 10;
const int16_t lightBlink = 1 << 11;
const int16_t lightBrightness = 0xff;

int blinkState = 0;
const int blinkSteps = 3;

unsigned long tempus;

void setup() {
  pinMode(8, OUTPUT); // Питание реостатов джойстика
  digitalWrite(8, HIGH);

  pinMode(9, OUTPUT); // Синий
  pinMode(10, OUTPUT); // Зелёный
  digitalWrite(9, HIGH);
  digitalWrite(10, HIGH);

  pinMode(A0, INPUT); // Горизонтальный джойстик
  pinMode(A1, INPUT); // Вертикальный джойстик
  
  pinMode(4, INPUT); // Кнопка
  digitalWrite(4, HIGH); // Подтягивающий резистор

  pinMode(2, OUTPUT); // Приём/передача
  digitalWrite(2, LOW);

  pinMode(13, OUTPUT); // Светодиод
  digitalWrite(13, LOW);
  tempus = millis() + 100; 
  digitalWrite(13, HIGH );
  
  slave.begin(9600);
  blinker.setInterval(100);

  modbusRegisters[0] = 0;
  modbusRegisters[1] = 5 << 8;
  modbusRegisters[2] = 0xffff;
  modbusRegisters[3] = 0;
}

int readDirection(int pin)
{
  const auto raw = analogRead(pin);
  if (raw >= movementCenter + minMovement)
    return map(constrain(raw - movementCenter - minMovement, 0, maxMovement), 0, maxMovement, 0, 255);
  else if (raw <= movementCenter - minMovement)
    return map(constrain(raw - movementCenter + minMovement, -maxMovement, 0), -maxMovement, 0, -255, 0);
  else
    return 0;
}

void loop() {
  thermometer.requestTemp();

  int direction = 5;
  int level = 0;
  if (digitalRead(4) == 0)
  {
    direction = 4;
    level = 255;
  }
  else
  {
    const auto horizontal = -readDirection(A0);
    const auto vertical = readDirection(A1);
    if (horizontal > 0 && horizontal / 2 > abs(vertical))
    {
      direction = (orientation + 1) % 4;
      level = horizontal;
    }
    else if (horizontal < 0 && -horizontal / 2 > abs(vertical))
    {
      direction = (orientation + 3) % 4;
      level = -horizontal;
    }
    else if (vertical > 0 && vertical / 2 > abs(horizontal))
    {
      direction = orientation;
      level = vertical;
    }
    else if (vertical < 0 && -vertical / 2 > abs(horizontal))
    {
      direction = (orientation + 2) % 4;
      level = -vertical;
    }
  }
  modbusRegisters[1] = ((direction << 8) | level);

  if (thermometer.readTemp())
    modbusRegisters[2] = thermometer.getTemp() * 100;

  const auto polled = slave.poll(modbusRegisters, modbusRegisterCount);
  if (polled > 4)
  {
    tempus = millis() + 100; 
    digitalWrite(13, HIGH );
  }
  if (millis() > tempus) digitalWrite(13, LOW );

  if (blinker.isReady())
  {
    if (++blinkState >= blinkSteps)
      blinkState = 0;
  }

  const auto light = modbusRegisters[3];
  const auto brightness = light & lightBrightness;
  const auto lightOn = brightness > 0 && ((light & lightBlink) ? blinkState != 0 : true);
  if (lightOn)
  {
    if (brightness == 255)
    {
      digitalWrite(9, (light & lightBlue) ? LOW : HIGH);
      digitalWrite(10, (light & lightGreen) ? LOW : HIGH);
    }
    else
    {
      analogWrite(9, (light & lightBlue) ? 255 - brightness : 255);
      analogWrite(10, (light & lightGreen) ? 255 - brightness : 255);
    }
  }
  else
  {
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
  }
}

// Управление светом: modpoll.exe -m rtu -a 21 -r 4 -c 1 -l 500 -b 9600 -p none COM4 <Значение>
// Опрос положения и температуры: modpoll.exe -m rtu -a 21 -r 2 -c 2 -l 500 -b 9600 -p none COM4
